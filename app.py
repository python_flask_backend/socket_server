from flasgger import Swagger
from flask import Flask, Blueprint
from flask_sockets import Sockets

html = Blueprint(r'html', __name__)


app = Flask(__name__)
sockets = Sockets(app)
swag = Swagger(app)

from videoSocket_con import videoStream

sockets.register_blueprint(videoStream)

if __name__ == "__main__":
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    server.serve_forever()
